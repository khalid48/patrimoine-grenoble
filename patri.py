import sqlite3
import pandas as pd
import numpy as np
import folium
import csv
from flask import Flask
#Pandas
data=pd.read_csv("PATRIMOINE_VDG.csv")
partimoine=data.copy(deep=True)
data_json=pd.read_json("PATRIMOINE_VDG_EPSG4326.json")
data_json['features']
#data_json["features"][0]["geometry"]
data_json["features"][0]
data_json["features"][0]["geometry"]["coordinates"]
latitude=[]
longitude=[]
titres=[]
for ele in data_json["features"]:
    latitude.append(ele["geometry"]["coordinates"][1])
    longitude.append(ele["geometry"]["coordinates"][0]) 
    
partimoine["Latitude"]=latitude
partimoine["Longitude"]=longitude

partimoine["Thématiques"]
#print(np.unique(partimoine['Thématiques']))
thématiques=[]
#Thématiques=partimoine["Thématiques"]
Thématiques=partimoine["Thématiques"].str.split(",",expand=True)
for ele in  Thématiques:
    if ele not in thématiques:
        thématiques.extend(partimoine["Thématiques"].str.split(","))
#sqlite3

try:
    sqliteConnection = sqlite3.connect('patrimoine.db')
    cursor = sqliteConnection.cursor()
    print("Database created and Successfully Connected to SQLite")

    sqlite_select_Query = "select sqlite_version();"
    cursor.execute(sqlite_select_Query)
    record = cursor.fetchall()
    print("SQLite Database Version is: ", record)
    cursor.close()

except sqlite3.Error as error:
    print("Error while connecting to sqlite", error)
finally:
    if (sqliteConnection):
        sqliteConnection.close()
        print("The SQLite connection is closed")
try:
    sqliteConnection = sqlite3.connect('patrimoine.db')
    sqlite_create_table_query = '''CREATE TABLE if not exists Patrimoine (
                                id INTEGER PRIMARY KEY,
                                Titre TEXT NOT NULL,
                                Thématiques TEXT NOT NULL,
                                Latitude FLOAT NOT NULL,
                                Longitude FLOAT NOT NULL);'''

    cursor = sqliteConnection.cursor()
    print("Successfully Connected to SQLite")
    cursor.execute(sqlite_create_table_query)
    sqliteConnection.commit()
    print("SQLite table created")

    cursor.close()
except sqlite3.Error as error:
    print("Error while creating a sqlite table", error)
finally:
    if (sqliteConnection):
        sqliteConnection.close()
        print("sqlite connection is closed") 
        


try:
    sqliteConnection = sqlite3.connect('patrimoine.db')
    cursor = sqliteConnection.cursor()
    print("Successfully Connected to SQLite")

    with open('/home/khalid/git/patrimoine_grenoble/patrimoine-grenoble/PATRIMOINE_VDG.csv', 'rt') as sqlite_file:
        sql_script = csv.DictReader(sqlite_file)
        
        for i in sql_script:
            to_db_partimo = [(i['Titre'],i['Thématiques'],i['Latitude'],i['Longitude'])]
            
            cursor.executemany("INSERT INTO Patrimoine (Titre,Thématiques,Latitude, Longitude) VALUES (?,?,?,?);", to_db_partimo)
            sqliteConnection.commit()
        print("SQLite script executed successfully")
        cursor.close()

except sqlite3.Error as error:
    print("Error while executing sqlite script", error)
finally:
    if (sqliteConnection):
        sqliteConnection.close()
        print("sqlite connection is closed")
def lire_SqTable():
    try:
        sqliteConnection = sqlite3.connect('patrimoine.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sqlite_select_query = """SELECT * from  Patrimoine """
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        print("Total rows are:  ", len(records))
        print("Printing each row")
        for row in records:
            print("id: ", row[0])
            print("Titre: ", row[1]) 
            print("Thématiques: ", row[2])
            print("Latitude: ", row[3])
            print("Longitude: ", row[4])
            
            print("\n")

        cursor.close()

    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("The SQLite connection is closed")

#lire_SqTable() 
        
def Tri_data():
    try:
        sqliteConnection = sqlite3.connect('patrimoine.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sqlite_select_query = """
                               SELECT 
                               id,
                               Titre,
                               Thématiques,
                               Latitude,Longitude 
                               from  Patrimoine
                               WHERE (Latitude = 0.0 AND Longitude = 0.0 )
                               
                               ;
                                 """
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        print("Total rows are:  ", len(records))
        print("Printing each row")
        for row in records:
            print("id: ", row[0])
            print("Titre: ", row[1]) 
            print("Latitude: ", row[2])
            print("Longitude: ", row[3])
            
            print("\n")

        cursor.close()

    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("The SQLite connection is closed")
lire_SqTable() 
#Tri_data()

app = Flask(__name__)

@app.route('/')
def index():
    try:
        map_titre= folium.Map(location=[45.16667, 5.71667],zoom_start=11)
        folium.raster_layers.TileLayer('Open Street Map').add_to(map_titre)
        folium.raster_layers.TileLayer('Stamen Terrain').add_to(map_titre)
        folium.raster_layers.TileLayer('Stamen Toner').add_to(map_titre)
        folium.raster_layers.TileLayer('Stamen Watercolor').add_to(map_titre)
        folium.raster_layers.TileLayer('CartoDB Positron').add_to(map_titre)
        folium.raster_layers.TileLayer('CartoDB Dark_Matter').add_to(map_titre)



        sqliteConnection = sqlite3.connect('patrimoine.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")
        sqlite_select_query = """SELECT Latitude,Longitude,Titre, Thématiques
                                FROM Patrimoine
                                 WHERE (Latitude != 0.0 AND Longitude != 0.0 )LIMIT 1000 ;
                                """
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        for row in records:
            lat = row[0]
            lon = row[1]
            titre = row[2]
            Thématiques=row[3]
            Nom = str(titre) +str(Thématiques)+ ": " + str(lat) + " , " + str(lon)
            #print(Nom)
            Nom = str(Thématiques)+str(titre)
            lgd_txt = '<span style="color: {col};">{txt}</span>'

            group01_Histoire = folium.FeatureGroup(name=lgd_txt.format(txt="Histoire & Evolution de la ville (Histoire.)", col="red"))
            group02_Art = folium.FeatureGroup(name=lgd_txt.format(txt="Art et Culture (Art.)", col="orange"))
            group03_science = folium.FeatureGroup(name=lgd_txt.format(txt="Sciences et Techniques (science.)", col="blue"))
            group04_mix = folium.FeatureGroup(name=lgd_txt.format(txt="Histoire & Evolution (mix.)", col="blueS"))
            

           
            
            if Thématiques=='Histoire & Evolution de la ville':
                icon = folium.Icon(color='red')
                group01_Histoire.add_to(map_titre)
            elif  Thématiques  =='Art et culture, Histoire & Evolution de la ville':
                 icon = folium.Icon(color='orange')
                 group02_Art.add_to(map_titre)
            elif  Thématiques  =='Histoire & Evolution de la ville, Sciences/tec...':       
                 icon = folium.Icon(color='blue')
                 group03_science.add(map_titre)
                
            else:
                icon = folium.Icon(color='green')
                group04_mix.add_to(map_titre)

            if lat!='' and lon!='' :
                folium.Marker([lat,lon],popup=Nom, icon=icon).add_to(map_titre)
            
        folium.LayerControl().add_to(map_titre)    
        map_titre.save(outfile='titre.html')

        print("Total rows are:  ", len(records))
        print("Printing each row")
        cursor.close()
        
    
    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("The SQLite connection is closed")
            return map_titre._repr_html_()

if __name__ == '__main__':
    app.run(port=5000)
#    app.run(debug=True)

        
      
        










