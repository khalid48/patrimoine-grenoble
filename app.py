from flask import Flask, request, render_template, flash, redirect, url_for, session, jsonify
import hashlib, uuid, os
from werkzeug.utils import secure_filename
import webbrowser
import time


app = Flask(__name__)


app.config.update(
    DEBUG=True,
    SECRET_KEY='secret_key'
)



@app.route('/')
def street():
	return render_template("titre.html")  

if __name__ == "__main__":
    app.run(host='localhost', debug=True)   


